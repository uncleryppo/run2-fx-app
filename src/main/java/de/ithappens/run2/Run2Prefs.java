package de.ithappens.run2;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.jasypt.util.text.BasicTextEncryptor;

import de.ithappens.commons.usercontext.EnvironmentPreference;
import de.ithappens.run2.database.DataSourceConnectionData;

public class Run2Prefs extends EnvironmentPreference {

	private final static String RUN2_DB_DRIVER = "run2-db-driver";
	private final static String RUN2_DB_NAME_DEV = "run2-db-name-dev";
	private final static String RUN2_DB_LOCATION_DEV = "run2-db-location-dev";
	private final static String RUN2_DB_SCHEME_DEV = "run2-db-scheme-dev";
	private final static String RUN2_DB_USERNAME_DEV = "run2-db-username-dev";
	private final static String RUN2_DB_PASSWORD_ENCRYPTED_DEV = "run2-db-password-encrypted-dev";
	private final static String RUN2_DB_NAME_PROD = "run2-db-name-prod";
	private final static String RUN2_DB_LOCATION_PROD = "run2-db-location-prod";
	private final static String RUN2_DB_SCHEME_PROD = "run2-db-scheme-prod";
	private final static String RUN2_DB_USERNAME_PROD = "run2-db-username-prod";
	private final static String RUN2_DB_PASSWORD_ENCRYPTED_PROD = "run2-db-password-encrypted-prod";
	private final static String RUN2_APP_MODE = "run2-app-mode";

	@Override
	public CONTEXT getContext() {
		return CONTEXT.USER;
	}

	private static final String CONFIG_FILE_NAME = "run2.config";
	private final Properties configFile = new Properties();

	public enum APP_MODE {
		DEV, PROD
	}

	private APP_MODE currentAppMode = APP_MODE.DEV;

	public Run2Prefs() throws FileNotFoundException, IOException {
		configFile.load(new FileReader(CONFIG_FILE_NAME));
	}

	public DataSourceConnectionData getRun2DatabaseConnectionData() {
		String driver_key = RUN2_DB_DRIVER;
		String name_key = RUN2_DB_NAME_DEV;
		String location_key = RUN2_DB_LOCATION_DEV;
		String scheme_key = RUN2_DB_SCHEME_DEV;
		String username_key = RUN2_DB_USERNAME_DEV;
		String encryptedPassword_key = RUN2_DB_PASSWORD_ENCRYPTED_DEV;
		if (APP_MODE.PROD.equals(currentAppMode)) {
			name_key = RUN2_DB_NAME_PROD;
			location_key = RUN2_DB_LOCATION_PROD;
			scheme_key = RUN2_DB_SCHEME_PROD;
			username_key = RUN2_DB_USERNAME_PROD;
			encryptedPassword_key = RUN2_DB_PASSWORD_ENCRYPTED_PROD;
		}
		return new DataSourceConnectionData(
				getConfigString(name_key),
				getConfigString(driver_key), 
				getConfigString(location_key), 
				getConfigString(scheme_key), 
				getConfigString(username_key), 
				decrypt(getConfigString(encryptedPassword_key)));
	}

	private String decrypt(String valueToDecrypt) {
		return StringUtils.isNotEmpty(valueToDecrypt) ? getEncryptor().decrypt(valueToDecrypt) : null;
	}

	private String encrypt(String valueToEncrypt) {
		return getEncryptor().encrypt(valueToEncrypt);
	}

	private BasicTextEncryptor getEncryptor() {
		BasicTextEncryptor ENCRYPTORncryptor = new BasicTextEncryptor();
		String APPLICATION_CRYPT_KEY = "The.Run.2-foo-bar"; // 16-bytes-length
		ENCRYPTORncryptor.setPassword(APPLICATION_CRYPT_KEY);
		return ENCRYPTORncryptor;
	}

	private String getConfigString(String key) {
		return configFile.getProperty(key);
	}

	public String getCurrentAppModeName() {
		return currentAppMode != null ? currentAppMode.name() : null;
	}

	public void loadCurrentAppMode() {
		try {
			currentAppMode = APP_MODE.valueOf(getConfigString(RUN2_APP_MODE));
		} catch (IllegalArgumentException iae) {
			currentAppMode = APP_MODE.DEV;
			LogManager.getLogger().error("Configured application mode '" + getConfigString(RUN2_APP_MODE)
					+ "' unknown. Fall back to " + currentAppMode.name() + ".", iae);
		}
	}

}
