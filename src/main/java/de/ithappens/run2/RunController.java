package de.ithappens.run2;

import java.io.IOException;

import com.dlsc.workbenchfx.Workbench;

import de.ithappens.run2.core.Run2AppController;
import de.ithappens.run2.fx.FxTools;
import de.ithappens.run2.module.CompetitionModule;
import de.ithappens.run2.module.DisciplineModule;
import javafx.fxml.FXML;
import javafx.scene.image.Image;

public class RunController extends Run2AppController {
	
	@FXML
	private Workbench workbench;

	@Override
	public void registerModules() {
		try {
			register(SCOPE.COMPETITION, new CompetitionModule(this));
			register(SCOPE.DISCIPLINE, new DisciplineModule(this));
			// register(SCOPE.CONTACT, new ContactModule(this));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public Image getImage() {
		return FxTools.APP_IMAGE__RUN2FXAPP;
	}

	@Override
	public String getAboutFxml() {
		return "about.fxml";
	}

	@Override
	public String getApplicationTitle() {
		return FxTools.RUN2_FX_APP;
	}

	@Override
	public SCOPE getDefaultScope() {
		return SCOPE.COMPETITION;
	}
	
//	private void testDb() throws SQLException {
//		org.apache.derby.jdbc.ClientConnectionPoolDataSource cpds = 
//				   new ClientConnectionPoolDataSource();
//
//
//				// Set other DataSource properties
////				cpds.setDatabaseName("APP");
//				cpds.setDatabaseName("y3jrun_dev");
//				cpds.setUser("null");
//				cpds.setPassword("null"); 
//				cpds.setServerName("localhost");
////				cpds.setDataSourceName("y3jrun_dev");
////				cpds.setDataSourceName("Y3JRUN");
//				cpds.setPortNumber(1527);
//
//				// This physical connection will have JDBC statement caching enabled.
//				java.sql.Connection pc = cpds.getConnection();
//
//
//				// Interact with the database.
//				java.sql.PreparedStatement ps = pc.prepareStatement(
//				   "select Y3JRUN.COMPETITION.ID from Y3JRUN.COMPETITION ");
//				ps.close(); // Inserts or returns statement to the cache
//				pc.close();
//	}

}
