package de.ithappens.run2;

import com.jpro.webapi.JProApplication;

import de.ithappens.commons.usercontext.UserContext;
import de.ithappens.run2.fx.FxTools;
import javafx.embed.swing.SwingFXUtils;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class TheRun2 extends JProApplication {

	public TheRun2() {
		super();
	}

	public static void main(String[] args) {
		launch(args);
	}

	@SuppressWarnings("restriction")
	@Override
	public void start(Stage primaryStage) throws Exception {
		try {
			UserContext.getInstance().addEnvironment(new Run2Prefs());
			getWebAPI();
			FXMLLoader loader = new FXMLLoader(getClass().getResource("workbench.fxml"));
			Parent root = loader.load();
			root.getStylesheets().add(getClass().getResource("run2.css").toExternalForm());
			Scene myScene = new Scene(root);
			primaryStage.setScene(myScene);
			primaryStage.setMaximized(true);
			primaryStage.setTitle(FxTools.RUN2);
			primaryStage.getIcons().add(FxTools.APP_IMAGE__RUN2FXAPP);
			primaryStage.show();

			if (isOSX()) {
				System.setProperty("apple.awt.graphics.EnableQ2DX", "true");
				System.setProperty("com.apple.mrj.application.apple.menu.about.name", FxTools.RUN2_FX_APP);
				System.setProperty("apple.laf.useScreenMenuBar", "true");
				com.apple.eawt.Application.getApplication().setDockIconBadge(FxTools.RUN2);
				com.apple.eawt.Application.getApplication()
						.setDockIconImage(SwingFXUtils.fromFXImage(FxTools.APP_IMAGE__RUN2FXAPP, null));
			}

			RunController controller = loader.getController();
			controller.registerModules();
			controller.open(controller.getDefaultScope());
		} catch (Exception ex) {
			ex.printStackTrace();
			System.exit(1);
		}

	}

	public static boolean isOSX() {
		String osName = System.getProperty("os.name").toLowerCase();
		return osName.contains("mac");
	}

}
