package de.ithappens.run2.fx;

import de.ithappens.run2.database.model.generated.tables.pojos.Ageclassesdefinition;
import javafx.util.StringConverter;

public class AgeClassesDefinitionStringConverter extends StringConverter<Ageclassesdefinition> {

	@Override
	public Ageclassesdefinition fromString(String string) {
		Ageclassesdefinition acd = new Ageclassesdefinition();
		acd.setName(string);
		return acd;
	}
	@Override
	public String toString(Ageclassesdefinition object) {
		return object == null ? null : object.getName();
	}

}
