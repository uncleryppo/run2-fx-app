package de.ithappens.run2.fx;

import java.awt.Desktop;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class FxTools {
	
	static Logger log = LogManager.getLogger();
	
	private final static String BUNDLE_NAME = "de.ithappens.run2";
	private final static ResourceBundle RB = ResourceBundle.getBundle(BUNDLE_NAME);
	
	private final static SimpleDateFormat DF = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss.SSS");
	
	public final static Image APP_IMAGE__RUN2FXAPP = getAppIcon("medal.png");
	
	public final static String FILEMANAGEMENT = get("FILEMANAGEMENT");
	public final static String COMPETITIONS = get("COMPETITIONS");
	public final static String DISCIPLINE = get("DISCIPLINE");
	public final static String CONTACTS = get("CONTACTS");
	public final static String ADD_FILE = get("ADD_FILE");
	public final static String SEARCH = get("SEARCH");
	public final static String CONNECT = get("CONNECT");
	public final static String CONNECTED = get("CONNECTED");
	public final static String RUN2_FX_APP = get("RUN2_FX_APP");
	public final static String APPLICATION_VERSION = get("APPLICATION_VERSION");
	public final static String RUN2 = get("RUN2");
	public final static String REFRESH = get("REFRESH");
	public final static String SAVE = get("SAVE");
	
	private final static String FX_LOCATION = "de/ithappens/run2/fx/";
	
	public final static Image ICON__COMPETITION = loadImage("flag-2.png");
	public final static Image ICON__DISCIPLINE = loadImage("flag-blue.png");
	public final static Image ICON__CONTACT = loadImage("identity.png");
	public final static Image ICON__SAVE = loadImage("games-endturn.png");
	public final static Image ICON__REFRESH = loadImage("view-refresh-3.png");

	public static Image loadImage(String filename) {
		return new Image(FX_LOCATION + filename);
	}

	public static Parent loadFxml(FXMLLoader loader) {
		Parent parent = null;
		try {
			parent = (Parent) loader.load();
		} catch (IOException iox) {
			iox.printStackTrace();
			log.error(iox);
		}
		return parent;
	}
	
	@SuppressWarnings("finally")
	private static Image getAppIcon(String imageFilename) {
		Image theImage = null;
		ClassLoader classLoader = FxTools.class.getClassLoader();
		try {
			theImage = new Image(classLoader.getResource(FX_LOCATION + imageFilename).openStream());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			return theImage;
		}
	}
	
	public static FXMLLoader getFXMLLoader(String fxmlFilename) {
		FXMLLoader loader = null;
		try {
			ClassLoader classLoader = FxTools.class.getClassLoader();
			String bundleFilename = StringUtils.replace(BUNDLE_NAME, ".", "/") + ".properties";
			log.debug("bundleFilename: " + bundleFilename);
			loader = new FXMLLoader(classLoader.getResource(FX_LOCATION + fxmlFilename),
					new PropertyResourceBundle(classLoader.getResource(bundleFilename).openStream()));
		} catch (IOException iox) {
			iox.printStackTrace();
			log.error(iox);
		}
		return loader;
	}
	
	@SuppressWarnings("finally")
	private static String get(String key) {
		String value = null;
		try {
			value = RB.getString(key);
		} catch (Exception ex) {
			ex.printStackTrace();
			value = "<TEXT_ERROR>";
		} finally {
			if (value == null || value.equals("")) {
				value = key;
			}
			return StringEscapeUtils.unescapeJava(value);
		}
	}

	public static void openWebPage(String siteUrl) {
		if (StringUtils.isNotEmpty(siteUrl)) {
			try {
				Desktop.getDesktop().browse(new URL(siteUrl).toURI());
			} catch (IOException | URISyntaxException e) {
				e.printStackTrace();
			}
		}
	}

	public static Button createButton(String labelText, String tooltipText, Image icon) {
		Button b = new Button(labelText, new ImageView(icon));
		if (StringUtils.isNotEmpty(tooltipText)) {
			b.setTooltip(new Tooltip(tooltipText));
		}
		return b;
	}

}
