package de.ithappens.run2.fx;

import de.ithappens.run2.database.model.generated.tables.pojos.Competition;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.util.Callback;

public class CompetitionCellFactory implements Callback<ListView<Competition>, ListCell<Competition>> {

	@Override
	public ListCell<Competition> call(ListView<Competition> param) {
		return new CompetitionListCell();
	}

}
