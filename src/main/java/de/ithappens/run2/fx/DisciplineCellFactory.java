package de.ithappens.run2.fx;

import de.ithappens.run2.database.model.generated.tables.pojos.Discipline;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.util.Callback;

public class DisciplineCellFactory implements Callback<ListView<Discipline>, ListCell<Discipline>> {

	@Override
	public ListCell<Discipline> call(ListView<Discipline> param) {
		return new DisciplineListCell();
	}

}