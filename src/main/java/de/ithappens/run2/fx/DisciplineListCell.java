package de.ithappens.run2.fx;

import de.ithappens.run2.database.model.generated.tables.pojos.Discipline;
import javafx.scene.control.ListCell;

public class DisciplineListCell extends ListCell<Discipline> {

	@Override
	protected void updateItem(Discipline discipline, boolean empty) {
		super.updateItem(discipline, empty);
		if (empty || discipline == null) {
			setText(null);
		} else {
			setText(discipline.getLength() + " " + discipline.getName());
		}
	}

}
