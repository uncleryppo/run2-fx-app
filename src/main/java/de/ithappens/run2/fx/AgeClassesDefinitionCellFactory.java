package de.ithappens.run2.fx;

import de.ithappens.run2.database.model.generated.tables.pojos.Ageclassesdefinition;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.util.Callback;

public class AgeClassesDefinitionCellFactory
		implements Callback<ListView<Ageclassesdefinition>, ListCell<Ageclassesdefinition>> {

	@Override
	public ListCell<Ageclassesdefinition> call(ListView<Ageclassesdefinition> param) {
		return new AgeClassesDefinitionListCell();
	}

}
