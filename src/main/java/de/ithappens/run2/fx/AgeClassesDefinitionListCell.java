package de.ithappens.run2.fx;

import de.ithappens.run2.database.model.generated.tables.pojos.Ageclassesdefinition;
import javafx.scene.control.ListCell;

public class AgeClassesDefinitionListCell extends ListCell<Ageclassesdefinition> {

	@Override
	protected void updateItem(Ageclassesdefinition ageclassesdefinition, boolean empty) {
		super.updateItem(ageclassesdefinition, empty);
		if (empty || ageclassesdefinition == null) {
			setText(null);
		} else {
			setText(ageclassesdefinition.getName());
		}
	}

}
