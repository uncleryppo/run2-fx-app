package de.ithappens.run2.fx;

import java.util.Calendar;

import de.ithappens.run2.database.model.generated.tables.pojos.Competition;
import javafx.scene.control.ListCell;

public class CompetitionListCell extends ListCell<Competition> {

	@Override
	protected void updateItem(Competition competition, boolean empty) {
		super.updateItem(competition, empty);
		if (empty || competition == null) {
			setText(null);
		} else {
			Calendar cal = Calendar.getInstance();
			cal.setTime(competition.getStartofcompetition());
			setText(cal.get(Calendar.YEAR) + ": " + competition.getTitle());
		}
	}

}
