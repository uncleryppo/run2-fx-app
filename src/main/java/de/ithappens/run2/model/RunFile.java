package de.ithappens.run2.model;

import java.io.File;

import org.apache.commons.lang3.StringUtils;

public class RunFile {

	private File theFile;
	private String theFilename;
	private String theFileContent;

	public File getTheFile() {
		return theFile;
	}

	public void setTheFile(File theFile) {
		this.theFile = theFile;
	}

	public void setTheFilename(String theFilename) {
		this.theFilename = theFilename;
	}

	public String getTheFilename() {
		return theFilename;
	}

	public String getTheFileContent() {
		return theFileContent;
	}

	public void setTheFileContent(String theFileContent) {
		this.theFileContent = theFileContent;
	}

	public int getSize() {
		return StringUtils.isNotEmpty(theFileContent) ? theFileContent.length() : 0;
	}

}
