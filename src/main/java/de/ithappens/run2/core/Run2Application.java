package de.ithappens.run2.core;

import com.jpro.webapi.JProApplication;

import de.ithappens.commons.usercontext.UserContext;
import de.ithappens.run2.Run2Prefs;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

public abstract class Run2Application extends JProApplication {

	public void appStart(Stage primaryStage) throws Exception {
		try {
			UserContext.getInstance().addEnvironment(new Run2Prefs());
			getWebAPI();
			FXMLLoader loader = new FXMLLoader(getClass().getResource(getFxmlFileName()));
			Parent root = loader.load();
			root.getStylesheets().add(getClass().getResource(getCssFileName()).toExternalForm());
			Scene theScene = new Scene(root);
			primaryStage.setTitle(getTitle());
			primaryStage.getIcons().add(getImage());
			primaryStage.setScene(theScene);
			primaryStage.setMaximized(true);
			primaryStage.show();
			
			Run2AppController appController = loader.getController();
			appController.initModules();
//			appController.open(appController.getDefaultScope());
			
		} catch (Exception ex) {
			ex.printStackTrace();
			System.exit(1);
		}
	}

	public abstract String getFxmlFileName();

	public abstract String getCssFileName();

	public abstract String getTitle();

	public abstract Image getImage();

}
