package de.ithappens.run2.core;

import java.awt.Desktop;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.dlsc.workbenchfx.Workbench;
import com.dlsc.workbenchfx.model.WorkbenchModule;
import com.dlsc.workbenchfx.view.controls.NavigationDrawer;
import com.dlsc.workbenchfx.view.controls.PrettyScrollPane;
import com.dlsc.workbenchfx.view.controls.ToolbarItem;
import com.jpro.webapi.WebAPI;

import de.ithappens.commons.usercontext.UserContext;
import de.ithappens.run2.Run2Prefs;
import de.ithappens.run2.database.RunDatabase;
import de.ithappens.run2.fx.FxTools;
import javafx.beans.Observable;
import javafx.beans.binding.Bindings;
import javafx.fxml.FXML;
import javafx.geometry.Side;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuButton;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Skin;
import javafx.scene.control.SkinBase;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TouchEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;

public abstract class Run2AppController {

	public enum SCOPE {
		DATASTORE, COMPETITION, CONTACT, PARTICIPATION, DISCIPLINE, AGECLASS
	}

	@FXML
	Workbench workbench;

	private Map<SCOPE, Run2AppContainer> run2Modules = new HashMap<>();

	private final static Run2Prefs PREFS = UserContext.getInstance().getEnvironment(Run2Prefs.class);

	private Logger log = LogManager.getLogger();
	private RunDatabase database;
	private Button dbConnection_btn;

	@FXML
	private void initialize() {
		VBox about_pne = (VBox) FxTools.loadFxml(FxTools.getFXMLLoader(getAboutFxml()));
		workbench.setNavigationDrawer(new CustomNavigationDrawer());
		MenuItem about_mi = new MenuItem(null, about_pne);
		about_mi.setOnAction((e) -> {
			// openUrl(PREFS.getDocumentationUrl());
		});
		workbench.getNavigationDrawerItems().add(about_mi);
		ImageView brand_imv = new ImageView(getImage());
		workbench.getToolbarControlsLeft().add(new ToolbarItem(brand_imv));
		dbConnection_btn = new Button(FxTools.CONNECT);
		dbConnection_btn.setOnAction((e) -> {
			getDatabase();
		});
		workbench.getToolbarControlsLeft().add(new ToolbarItem(dbConnection_btn));
		Label appTitle_lbl = new Label(getApplicationTitle() + " v" + FxTools.APPLICATION_VERSION);
		appTitle_lbl.setPrefSize(500d, 50d);
		appTitle_lbl.setMinSize(500d, 50d);
		appTitle_lbl.setMaxSize(500d, 50d);
		workbench.getToolbarControlsLeft().add(new ToolbarItem(appTitle_lbl));
	}

	public void initModules() {
		registerModules();
	}

	public void register(SCOPE scope, Run2Module module) throws IOException {
		workbench.getModules().add(module);
		module.init(workbench);
		Run2ModuleController<?> moduleController = module.getLoader().getController();
		run2Modules.put(scope, new Run2AppContainer(module, moduleController));
	}

	public abstract void registerModules();

	public abstract Image getImage();

	public abstract String getAboutFxml();

	public abstract String getApplicationTitle();

	public abstract SCOPE getDefaultScope();

	public void open(SCOPE scope, Object model) {
		Run2AppContainer appContainer = run2Modules.get(scope);
		appContainer.controller.openModel(model);
		workbench.openModule(appContainer.module);
	}

	public void open(SCOPE scope) {
		open(scope, null);
	}

	public void openUrl(String url) {
		if (StringUtils.isNotEmpty(url) && !WebAPI.isBrowser()) {
			try {
				Desktop.getDesktop().browse(new URI(url));
			} catch (IOException | URISyntaxException e) {
				log.error(e);
			}
		} else {
			WebAPI.getWebAPI(workbench.getScene()).executeScript("window.open('" + url + "', '_blank');");
		}
	}

	public RunDatabase getDatabase() {
		if (database == null) {
			try {
				database = new RunDatabase(
						PREFS.getRun2DatabaseConnectionData(), 
						FxTools.RUN2_FX_APP, 
						UserContext.getInstance().getOperatingSystemEnvironment().getUsername());
			} catch (InstantiationException | IllegalAccessException | ClassNotFoundException | SQLException e) {
				log.error(e);
				e.printStackTrace();
			}
		}
		if (database != null && database.isConnected()) {
			dbConnection_btn.setText(FxTools.CONNECTED);
			dbConnection_btn.setDisable(true);
		} else {
			WorkbenchModule activeModule = workbench.getActiveModule();
			if (activeModule != null) {
				workbench.getActiveModule().close();
			}
			dbConnection_btn.setText(FxTools.CONNECT);
			dbConnection_btn.setDisable(false);
		}
		return database;
	}

	class Run2AppContainer {

		Run2Module module;
		Run2ModuleController controller;

		public Run2AppContainer(Run2Module _module, Run2ModuleController _controller) {
			module = _module;
			controller = _controller;
		}

	}

	class CustomNavigationDrawer extends NavigationDrawer {
		/**
		 * Creates a navigation drawer control.
		 */
		public CustomNavigationDrawer() {
			super();
		}

		@Override
		protected Skin<?> createDefaultSkin() {
			return new CustomNavigationDrawerSkin(this);
		}
	}

	class CustomNavigationDrawerSkin extends SkinBase<CustomNavigationDrawer> {

		private VBox menuContainer;
		private CustomNavigationDrawer navigationDrawer;
		private VBox drawerBox;
		private BorderPane header;
		private PrettyScrollPane scrollPane;
		private StackPane backIconShape;
		private Button backBtn;
		private Label companyLogo;

		/**
		 * Creates the skin for the {@link CustomNavigationDrawer} control.
		 *
		 * @param navigationDrawer to create this skin for
		 */
		public CustomNavigationDrawerSkin(CustomNavigationDrawer navigationDrawer) {
			super(navigationDrawer);
			this.navigationDrawer = navigationDrawer;

			initializeParts();
			layoutParts();
			setupEventHandlers();
			setupValueChangedListeners();

			buildMenu();
		}

		/**
		 * Initializes all parts of the skin.
		 */
		private void initializeParts() {
			drawerBox = new VBox();
			drawerBox.getStyleClass().add("drawer-box");

			header = new BorderPane();
			header.getStyleClass().add("header");

			menuContainer = new VBox();
			menuContainer.getStyleClass().add("menu-container");

			scrollPane = new PrettyScrollPane(menuContainer);

			backIconShape = new StackPane();
			backIconShape.getStyleClass().add("shape");
			backBtn = new Button("", backIconShape);
			backBtn.getStyleClass().add("icon");
			backBtn.setId("back-button");

			companyLogo = new Label();
			companyLogo.getStyleClass().add("logo");
		}

		/**
		 * Defines the layout of all parts in the skin.
		 */
		private void layoutParts() {
			drawerBox.getChildren().addAll(header, scrollPane);

			menuContainer.setFillWidth(true);

			header.setTop(backBtn);
			header.setCenter(companyLogo);

			getChildren().add(drawerBox);
		}

		private void setupEventHandlers() {
			backBtn.setOnAction(evt -> navigationDrawer.hide());
		}

		private void setupValueChangedListeners() {
			navigationDrawer.getItems().addListener((Observable it) -> buildMenu());
		}

		private void buildMenu() {
			menuContainer.getChildren().clear();
			for (MenuItem item : getSkinnable().getItems()) {
				if (item instanceof Menu) {
					// item is a submenu
					menuContainer.getChildren().add(buildSubmenu(item));
				} else {
					// item is a regular menu item
					menuContainer.getChildren().add(buildMenuItem(item));
				}
			}
		}

		private MenuButton hoveredBtn;
		private boolean isTouchUsed = false;

		private MenuButton buildSubmenu(MenuItem item) {
			Menu menu = (Menu) item;
			MenuButton menuButton = new MenuButton();
			menuButton.setPopupSide(Side.RIGHT);
			menuButton.graphicProperty().bind(menu.graphicProperty());
			menuButton.textProperty().bind(menu.textProperty());
			menuButton.disableProperty().bind(menu.disableProperty());
			menuButton.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
			menuButton.getStyleClass().addAll(item.getStyleClass());
			Bindings.bindContent(menuButton.getItems(), menu.getItems());

			// To determine if a TOUCH_RELEASED event happens.
			// The MOUSE_ENTERED results in an unexpected behaviour on touch events.
			// Event filter triggers before the handler.
			menuButton.addEventFilter(TouchEvent.TOUCH_RELEASED, e -> isTouchUsed = true);

			// Only when ALWAYS or SOMETIMES
			if (!Priority.NEVER.equals(getSkinnable().getMenuHoverBehavior())) {
				menuButton.addEventHandler(MouseEvent.MOUSE_ENTERED, e -> { // Triggers on hovering over Menu
					if (isTouchUsed) {
						isTouchUsed = false;
						return;
					}
					// When ALWAYS, then trigger immediately. Else check if clicked before (case:
					// SOMETIMES)
					if (Priority.ALWAYS.equals(getSkinnable().getMenuHoverBehavior())
							|| (hoveredBtn != null && hoveredBtn.isShowing())) {
						menuButton.show(); // Shows the context-menu
						if (hoveredBtn != null && hoveredBtn != menuButton) {
							hoveredBtn.hide(); // Hides the previously hovered Button if not null and not self
						}
					}
					hoveredBtn = menuButton; // Add the button as previously hovered
				});
			}
			return menuButton;
		}

		private Button buildMenuItem(MenuItem item) {
			Button button = new Button();
			button.textProperty().bind(item.textProperty());
			button.graphicProperty().bind(item.graphicProperty());
			button.disableProperty().bind(item.disableProperty());
			button.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
			button.getStyleClass().addAll(item.getStyleClass());
			button.setOnAction(item.getOnAction());

			// Only in cases ALWAYS and SOMETIMES: hide previously hovered button
			if (!Priority.NEVER.equals(getSkinnable().getMenuHoverBehavior())) {
				button.addEventHandler(MouseEvent.MOUSE_ENTERED, e -> { // Triggers on hovering over Button
					if (!isTouchUsed) {
						if (hoveredBtn != null) {
							hoveredBtn.hide(); // Hides the previously hovered Button if not null
						}
						hoveredBtn = null; // Sets it to null
					}
				});
			}
			return button;
		}
	}

}
