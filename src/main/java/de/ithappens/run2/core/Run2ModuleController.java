package de.ithappens.run2.core;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import com.dlsc.workbenchfx.view.controls.ToolbarItem;

import de.ithappens.run2.database.RunDatabase;
import de.ithappens.run2.database.model.engine.EngineInterface;
import de.ithappens.run2.fx.FxTools;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;

public abstract class Run2ModuleController<T> {
	
	private Run2AppController mainController;
	private Run2Module gui;
	
	public void build(Run2AppController _mainController, Run2Module _gui) {
		gui = _gui;
		mainController = _mainController;
		gui.getToolbarControlsLeft().clear();
		gui.getToolbarControlsRight().clear();
		getModelsListView().getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
			resetForm();
			if (newValue != null) {
				openModel(newValue);
			}
		});
		buildUi();
	}
	
	public abstract void buildUi();
	
	protected Run2Module getGui() {
		return gui;
	}
	
	public void placeRefreshButton() {
		Button refresh_btn = FxTools.createButton(null, FxTools.REFRESH, FxTools.ICON__REFRESH);
		refresh_btn.setOnAction((e) -> {
			loadModels();
		});
		getGui().getToolbarControlsLeft().add(new ToolbarItem(refresh_btn));
	}

	public void placeSaveButton() {
		Button save_btn = FxTools.createButton(null, FxTools.SAVE, FxTools.ICON__SAVE);
		save_btn.setOnAction((e) -> {
			T model = getSelectedModel();
			saveModel(model);
			loadModels();
			getModelsListView().getSelectionModel().select(model);
		});
		getGui().getToolbarControlsLeft().add(new ToolbarItem(save_btn));
	}

	public T getSelectedModel() {
		return getModelsListView().getSelectionModel().getSelectedItem();
	}

	public void saveModel(T model) {
		if (model != null) {
			fillModelFromForm(model);
			getModelEngine().update(model);
		}
	}

	public abstract void resetForm();

	public abstract void fillModelFromForm(T model);

	public abstract EngineInterface<T> getModelEngine();

	public abstract void openModel(T model);
	
	public void loadModels() {
		T selectedModel = getSelectedModel();
		getModelsListView().getItems().clear();
		RunDatabase runDatabase = getMainController().getDatabase();
		if (runDatabase.isConnected()) {
			List<T> loadedModels = getModelEngine().findAll();
			if (CollectionUtils.isNotEmpty(loadedModels)) {
				getModelsListView().getItems().addAll(loadedModels);
			}
		}
		getModelsListView().getSelectionModel().select(selectedModel);
	}

	public abstract ListView<T> getModelsListView();

	protected Run2AppController getMainController() {
		return mainController;
	}

	public String toString(Timestamp timestamp) {
		return timestamp == null ? null : new SimpleDateFormat().format(timestamp);
	}

	@SuppressWarnings("finally")
	public Timestamp toTimestamp(String string) {
		Timestamp timestamp = null;
		try {
			Date date = StringUtils.isEmpty(string) ? null : new SimpleDateFormat().parse(string);
			if (date != null) {
				timestamp = new Timestamp(date.getTime());
			}
		} catch (ParseException e) {
			e.printStackTrace();
		} finally {
			return timestamp;
		}
	}

	public String toString(Integer integer) {
		return integer == null ? null : integer.toString();
	}

	@SuppressWarnings("finally")
	public Integer toInteger(String string) {
		Integer integer = null;
		try {
			if (StringUtils.isNotEmpty(string)) {
				integer = new Integer(string);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			return integer;
		}
	}

}
