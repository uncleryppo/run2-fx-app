package de.ithappens.run2.core;

import com.dlsc.workbenchfx.Workbench;
import com.dlsc.workbenchfx.model.WorkbenchModule;

import de.ithappens.run2.fx.FxTools;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import de.jensd.fx.glyphs.materialdesignicons.MaterialDesignIcon;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.image.Image;

public abstract class Run2Module extends WorkbenchModule {

	private Node ui;
	private FXMLLoader uiLoader;
	private String fxmlFileName;
	private Run2AppController appController;

	public Run2Module(String name, MaterialDesignIcon icon, String _fxmlFileName, Run2AppController _appController) {
		super(name, icon);
		fxmlFileName = _fxmlFileName;
		appController = _appController;
	}
	
	public Run2Module(String name, FontAwesomeIcon icon, String _fxmlFileName, Run2AppController _appController) {
		super(name, icon);
		fxmlFileName = _fxmlFileName;
		appController = _appController;
	}

	public Run2Module(String name, Image icon, String _fxmlFileName, Run2AppController _appController) {
		super(name, icon);
		fxmlFileName = _fxmlFileName;
		appController = _appController;
	}

	@Override
	public void init(Workbench workbench) {
		super.init(workbench);
		uiLoader = FxTools.getFXMLLoader(fxmlFileName);
		ui = FxTools.loadFxml(uiLoader);
		Run2ModuleController controller = uiLoader.getController();
		controller.build(appController, this);
	} 
	
	@Override
	public Node activate() {
		return ui;
	}
	
	public FXMLLoader getLoader() {
		return uiLoader;
	}

}
