package de.ithappens.run2.module;

import de.ithappens.run2.RunController;
import de.ithappens.run2.core.Run2Module;
import de.ithappens.run2.fx.FxTools;

public class ContactModule extends Run2Module {

	public ContactModule(RunController _mainController) {
		super(FxTools.CONTACTS, FxTools.ICON__CONTACT, "contacts.fxml", _mainController);
	}

}
