package de.ithappens.run2.module;

import de.ithappens.run2.RunController;
import de.ithappens.run2.core.Run2Module;
import de.ithappens.run2.fx.FxTools;

public class CompetitionModule extends Run2Module {
	
	public CompetitionModule(RunController _mainController) {
		super(FxTools.COMPETITIONS, FxTools.ICON__COMPETITION, "competitions.fxml", _mainController);
	}

}
