package de.ithappens.run2.module;

import de.ithappens.run2.core.Run2ModuleController;
import de.ithappens.run2.database.model.engine.EngineInterface;
import de.ithappens.run2.database.model.generated.tables.pojos.Discipline;
import de.ithappens.run2.fx.DisciplineCellFactory;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;

public class DisciplineController extends Run2ModuleController<Discipline> {

	@FXML
	private MetaDataController metadataController;

	@FXML
	private TextField searchDiscipline_txf, disciplineName_txf, length_txf, start_txf, shortName_txf, price_txf;
	@FXML
	private ListView<Discipline> disciplines_lvw;
	ObservableList<Discipline> disciplines_model;

	@FXML
	ComboBox<String> resultTimeFormat_cbx;

	@Override
	public void buildUi() {
		disciplines_model = FXCollections.observableArrayList();
		disciplines_lvw.setItems(disciplines_model);
		placeRefreshButton();
		placeSaveButton();
		disciplines_lvw.setCellFactory(new DisciplineCellFactory());
		resultTimeFormat_cbx.getItems().addAll("HH_mm_ss_SSS", "HH_mm_ss", "mm_ss");
		price_txf.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (newValue != null && !newValue.matches("\\d+(\\.\\d{1,2})?")) {
					price_txf.setText(oldValue);
				} else {
					price_txf.setText(newValue);
				}
			}
		});
	}

	@Override
	public void resetForm() {
		disciplineName_txf.setText(null);
		length_txf.setText(null);
		start_txf.setText(null);
		shortName_txf.setText(null);
		resultTimeFormat_cbx.getSelectionModel().clearSelection();
		price_txf.setText(null);
		metadataController.reset();
	}

	@Override
	public void openModel(Discipline model) {
		disciplineName_txf.setText(model.getName());
		length_txf.setText(model.getLength());
		start_txf.setText(toString(model.getStart()));
		shortName_txf.setText(model.getShortName());
		resultTimeFormat_cbx.getSelectionModel().select(model.getDisciplineTimeformat());
		price_txf.setText(toString(model.getPriceInEurocent()));
		metadataController.set(model);
	}

	@Override
	public ListView<Discipline> getModelsListView() {
		return disciplines_lvw;
	}

	@Override
	public void fillModelFromForm(Discipline model) {
		if (model != null) {
			model.setName(disciplineName_txf.getText());
			model.setShortName(shortName_txf.getText());
			model.setLength(length_txf.getText());
			model.setStart(toTimestamp(start_txf.getText()));
			model.setDisciplineTimeformat(resultTimeFormat_cbx.getSelectionModel().getSelectedItem());
			model.setPriceInEurocent(toInteger(price_txf.getText()));
		}

	}

	@Override
	public EngineInterface<Discipline> getModelEngine() {
		return getMainController().getDatabase().DISCIPLINE_engine();
	}

}
