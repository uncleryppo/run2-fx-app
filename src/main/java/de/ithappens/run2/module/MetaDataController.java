package de.ithappens.run2.module;

import de.ithappens.run2.database.model.generated.tables.pojos.Competition;
import de.ithappens.run2.database.model.generated.tables.pojos.Discipline;
import javafx.fxml.FXML;
import javafx.scene.control.Label;

public class MetaDataController {

	@FXML
	private Label id_lbl, version_lbl, importFingerprint_lbl, changer_lbl;

	public void set(Discipline discipline) {
		if (discipline != null) {
			set(discipline.getId(), discipline.getVersion(), discipline.getImportFingerprint(),
					discipline.getChanger());
		} else {
			reset();
		}
	}

	public void set(Competition competition) {
		if (competition != null) {
			set(competition.getId(), competition.getVersion(), competition.getImportFingerprint(),
					competition.getChanger());
		} else {
			reset();
		}
	}

	public void set(Integer id, Integer version, String importFingerprint, String changer) {
		set(id != null ? Integer.toString(id) : null, version != null ? Integer.toString(version) : null,
				importFingerprint, changer);
	}

	public void set(String id, String version, String importFingerprint, String changer) {
		id_lbl.setText(id);
		version_lbl.setText(version);
		importFingerprint_lbl.setText(importFingerprint);
		changer_lbl.setText(changer);
	}

	public void reset() {
		id_lbl.setText(null);
		version_lbl.setText(null);
		importFingerprint_lbl.setText(null);
		changer_lbl.setText(null);
	}

}
