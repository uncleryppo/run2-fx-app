package de.ithappens.run2.module;

import de.ithappens.run2.RunController;
import de.ithappens.run2.core.Run2Module;
import de.ithappens.run2.fx.FxTools;

public class DisciplineModule extends Run2Module {

	public DisciplineModule(RunController _mainController) {
		super(FxTools.DISCIPLINE, FxTools.ICON__DISCIPLINE, "discipline.fxml", _mainController);
	}

}
