package de.ithappens.run2.module;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;

import de.ithappens.run2.core.Run2ModuleController;
import de.ithappens.run2.database.RunDatabase;
import de.ithappens.run2.database.model.engine.EngineInterface;
import de.ithappens.run2.database.model.generated.tables.pojos.Ageclassesdefinition;
import de.ithappens.run2.database.model.generated.tables.pojos.Competition;
import de.ithappens.run2.fx.AgeClassesDefinitionCellFactory;
import de.ithappens.run2.fx.AgeClassesDefinitionStringConverter;
import de.ithappens.run2.fx.CompetitionCellFactory;
import de.ithappens.run2.fx.FxTools;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;

public class CompetitionController extends Run2ModuleController<Competition> {

	@FXML
	private MetaDataController metadataController;

	@FXML
	private TextField searchCompetition_txf, competitionTitle_txf, competitionStart_txf, competitionEnd_txf,
			competitionHomepage_txf, competitionRegistrationPage_txf, competitionRegistrationFile_txf;

	@FXML
	private ListView<Competition> competitions_lvw;
	ObservableList<Competition> competitions_model;

	@FXML
	Label competitionId_lbl;

	@FXML
	ComboBox<Ageclassesdefinition> ageClassesDefinition_cbx;

	@Override
	public void buildUi() {
		competitions_model = FXCollections.observableArrayList();
		competitions_lvw.setItems(competitions_model);
		placeRefreshButton();
		placeSaveButton();

		competitions_lvw.setCellFactory(new CompetitionCellFactory());
		ageClassesDefinition_cbx.setCellFactory(new AgeClassesDefinitionCellFactory());
		ageClassesDefinition_cbx.setConverter(new AgeClassesDefinitionStringConverter());
	}

	@Override
	public void resetForm() {
		competitionId_lbl.setText(null);
		competitionTitle_txf.setText(null);
		competitionStart_txf.setText(null);
		competitionEnd_txf.setText(null);
		competitionHomepage_txf.setText(null);
		competitionRegistrationPage_txf.setText(null);
		competitionRegistrationFile_txf.setText(null);
		ageClassesDefinition_cbx.getItems().clear();
		ageClassesDefinition_cbx.getSelectionModel().clearSelection();
		metadataController.reset();
	}

	public void actionOpenCompetitionHomepage() {
		FxTools.openWebPage(competitionHomepage_txf.getText());
	}

	public void actionOpenCompetitionRegistrationPage() {
		FxTools.openWebPage(competitionRegistrationPage_txf.getText());
	}

	public void actionOpenCompetitionRegistrationFile() {
		FxTools.openWebPage(competitionRegistrationFile_txf.getText());
	}

	@Override
	public void openModel(Competition model) {
		if (model != null) {
			competitionId_lbl.setText(Integer.toString(model.getId()));
			competitionTitle_txf.setText(model.getTitle());
			competitionStart_txf.setText(toString(model.getStartofcompetition()));
			competitionEnd_txf.setText(toString(model.getEndofcompetition()));
			competitionHomepage_txf.setText(model.getWebPage());
			competitionRegistrationPage_txf.setText(model.getRegistrationPage());
			competitionRegistrationFile_txf.setText(model.getRegistrationsLogFile());
			RunDatabase runDatabase = getMainController().getDatabase();
			if (runDatabase.isConnected()) {
				List<Ageclassesdefinition> ageclassesdefinitions = runDatabase.AGE_CLASSES_DEFINITION_engine().DAO()
						.findAll();
				if (CollectionUtils.isNotEmpty(ageclassesdefinitions)) {
					ageClassesDefinition_cbx.setItems(FXCollections.observableArrayList());
					ageClassesDefinition_cbx.getItems().addAll(ageclassesdefinitions);
					Ageclassesdefinition ageclassesdefinition = runDatabase.AGE_CLASSES_DEFINITION_engine().DAO()
							.fetchOneById(model.getRelatedageclassesdefinitionId());
					ageClassesDefinition_cbx.getSelectionModel().select(ageclassesdefinition);
				}
			}
			metadataController.set(model);
		}
	}

	@Override
	public ListView<Competition> getModelsListView() {
		return competitions_lvw;
	}

	@Override
	public void fillModelFromForm(Competition competition) {
		if (competition != null) {
			competition.setTitle(competitionTitle_txf.getText());
			competition.setRegistrationPage(competitionRegistrationPage_txf.getText());
			competition.setRegistrationsLogFile(competitionRegistrationFile_txf.getText());
			competition.setWebPage(competitionHomepage_txf.getText());
			competition.setStartofcompetition(toTimestamp(competitionStart_txf.getText()));
			competition.setEndofcompetition(toTimestamp(competitionEnd_txf.getText()));
			competition.setRelatedageclassesdefinitionId(
					ageClassesDefinition_cbx.getSelectionModel().getSelectedItem() == null ? null
							: ageClassesDefinition_cbx.getSelectionModel().getSelectedItem().getId());
		}
	}

	@Override
	public EngineInterface<Competition> getModelEngine() {
		return getMainController().getDatabase().COMPETITION_engine();
	}

}
